﻿namespace Sisfv.EnvironmentsCatalog.Domain.Entities;

public class Catalog : BaseAuditableEntity
{
    public string? Title { get; set; }

    public Colour Colour { get; set; } = Colour.White;

    public IList<CatalogItem> Items { get; private set; } = new List<CatalogItem>();
}
