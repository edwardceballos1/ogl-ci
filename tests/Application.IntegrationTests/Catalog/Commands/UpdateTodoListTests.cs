﻿using Sisfv.EnvironmentsCatalog.Application.Common.Exceptions;
using Sisfv.EnvironmentsCatalog.Application.Catalogs.Commands.CreateCatalog;
using Sisfv.EnvironmentsCatalog.Application.Catalogs.Commands.UpdateCatalog;
using Sisfv.EnvironmentsCatalog.Domain.Entities;
using FluentAssertions;
using NUnit.Framework;

namespace Sisfv.EnvironmentsCatalog.Application.IntegrationTests.Catalogs.Commands;

using static Testing;

public class UpdateCatalogTests : BaseTestFixture
{
    [Test]
    public async Task ShouldRequireValidCatalogId()
    {
        var command = new UpdateCatalogCommand { Id = 99, Title = "New Title" };
        await FluentActions.Invoking(() => SendAsync(command)).Should().ThrowAsync<NotFoundException>();
    }

    [Test]
    public async Task ShouldRequireUniqueTitle()
    {
        var listId = await SendAsync(new CreateCatalogCommand
        {
            Title = "New List"
        });

        await SendAsync(new CreateCatalogCommand
        {
            Title = "Other List"
        });

        var command = new UpdateCatalogCommand
        {
            Id = listId,
            Title = "Other List"
        };

        (await FluentActions.Invoking(() =>
            SendAsync(command))
                .Should().ThrowAsync<ValidationException>().Where(ex => ex.Errors.ContainsKey("Title")))
                .And.Errors["Title"].Should().Contain("The specified title already exists.");
    }

    [Test]
    public async Task ShouldUpdateCatalog()
    {
        var userId = await RunAsDefaultUserAsync();

        var listId = await SendAsync(new CreateCatalogCommand
        {
            Title = "New List"
        });

        var command = new UpdateCatalogCommand
        {
            Id = listId,
            Title = "Updated List Title"
        };

        await SendAsync(command);

        var list = await FindAsync<Catalog>(listId);

        list.Should().NotBeNull();
        list!.Title.Should().Be(command.Title);
        list.LastModifiedBy.Should().NotBeNull();
        list.LastModifiedBy.Should().Be(userId);
        list.LastModified.Should().NotBeNull();
        list.LastModified.Should().BeCloseTo(DateTime.Now, TimeSpan.FromMilliseconds(10000));
    }
}
