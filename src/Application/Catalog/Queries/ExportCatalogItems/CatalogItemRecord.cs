﻿using Sisfv.EnvironmentsCatalog.Application.Common.Mappings;
using Sisfv.EnvironmentsCatalog.Domain.Entities;

namespace Sisfv.EnvironmentsCatalog.Application.Catalogs.Queries.ExportCatalogItems;

public class CatalogItemRecord : IMapFrom<CatalogItem>
{
    public string? Title { get; set; }

    public bool Done { get; set; }
}
