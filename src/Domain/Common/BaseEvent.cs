﻿using MediatR;

namespace Sisfv.EnvironmentsCatalog.Domain.Common;

public abstract class BaseEvent : INotification
{
}
