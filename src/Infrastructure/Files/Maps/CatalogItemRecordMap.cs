﻿using System.Globalization;
using CsvHelper.Configuration;
using Sisfv.EnvironmentsCatalog.Application.Catalogs.Queries.ExportCatalogItems;

namespace Sisfv.EnvironmentsCatalog.Infrastructure.Files.Maps;

public class CatalogItemRecordMap : ClassMap<CatalogItemRecord>
{
    public CatalogItemRecordMap()
    {
        AutoMap(CultureInfo.InvariantCulture);

       Map(m => m.Done).ConvertUsing(c => c.Done ? "Yes" : "No");
    }
}
