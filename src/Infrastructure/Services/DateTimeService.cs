﻿using Sisfv.EnvironmentsCatalog.Application.Common.Interfaces;

namespace Sisfv.EnvironmentsCatalog.Infrastructure.Services;

public class DateTimeService : IDateTime
{
    public DateTime Now => DateTime.Now;
}
