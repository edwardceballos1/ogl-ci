﻿namespace Sisfv.EnvironmentsCatalog.Application.Catalogs.Queries.ExportCatalogItems;

public class ExportCatalogItemsVm
{
    public ExportCatalogItemsVm(string fileName, string contentType, byte[] content)
    {
        FileName = fileName;
        ContentType = contentType;
        Content = content;
    }

    public string FileName { get; set; }

    public string ContentType { get; set; }

    public byte[] Content { get; set; }
}
