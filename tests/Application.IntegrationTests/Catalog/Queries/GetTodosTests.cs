﻿using Sisfv.EnvironmentsCatalog.Application.Catalogs.Queries.GetTodos;
using Sisfv.EnvironmentsCatalog.Domain.Entities;
using Sisfv.EnvironmentsCatalog.Domain.ValueObjects;
using FluentAssertions;
using NUnit.Framework;

namespace Sisfv.EnvironmentsCatalog.Application.IntegrationTests.Catalogs.Queries;

using static Testing;

public class GetTodosTests : BaseTestFixture
{
    [Test]
    public async Task ShouldReturnPriorityLevels()
    {
        await RunAsDefaultUserAsync();

        var query = new GetTodosQuery();

        var result = await SendAsync(query);

        result.PriorityLevels.Should().NotBeEmpty();
    }

    [Test]
    public async Task ShouldReturnAllListsAndItems()
    {
        await RunAsDefaultUserAsync();

        await AddAsync(new Catalog
        {
            Title = "Shopping",
            Colour = Colour.Blue,
            Items =
                    {
                        new CatalogItem { Title = "Apples", Done = true },
                        new CatalogItem { Title = "Milk", Done = true },
                        new CatalogItem { Title = "Bread", Done = true },
                        new CatalogItem { Title = "Toilet paper" },
                        new CatalogItem { Title = "Pasta" },
                        new CatalogItem { Title = "Tissues" },
                        new CatalogItem { Title = "Tuna" }
                    }
        });

        var query = new GetTodosQuery();

        var result = await SendAsync(query);

        result.Lists.Should().HaveCount(1);
        result.Lists.First().Items.Should().HaveCount(7);
    }

    [Test]
    public async Task ShouldDenyAnonymousUser()
    {
        var query = new GetTodosQuery();

        var action = () => SendAsync(query);
        
        await action.Should().ThrowAsync<UnauthorizedAccessException>();
    }
}
