﻿using NUnit.Framework;

namespace Sisfv.EnvironmentsCatalog.Application.IntegrationTests;

using static Testing;

[TestFixture]
public abstract class BaseTestFixture
{
    [SetUp]
    public async Task TestSetUp()
    {
        await ResetState();
    }
}
