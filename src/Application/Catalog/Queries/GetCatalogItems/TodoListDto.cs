﻿using Sisfv.EnvironmentsCatalog.Application.Common.Mappings;
using Sisfv.EnvironmentsCatalog.Domain.Entities;

namespace Sisfv.EnvironmentsCatalog.Application.Catalogs.Queries.GetTodos;

public class CatalogDto : IMapFrom<Catalog>
{
    public CatalogDto()
    {
        Items = new List<CatalogItemDto>();
    }

    public int Id { get; set; }

    public string? Title { get; set; }

    public string? Colour { get; set; }

    public IList<CatalogItemDto> Items { get; set; }
}
