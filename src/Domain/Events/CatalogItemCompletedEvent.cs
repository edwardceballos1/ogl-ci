﻿namespace Sisfv.EnvironmentsCatalog.Domain.Events;

public class CatalogItemCompletedEvent : BaseEvent
{
    public CatalogItemCompletedEvent(CatalogItem item)
    {
        Item = item;
    }

    public CatalogItem Item { get; }
}
