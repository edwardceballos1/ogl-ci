﻿using Sisfv.EnvironmentsCatalog.Application.Common.Exceptions;
using Sisfv.EnvironmentsCatalog.Application.CatalogItems.Commands.CreateCatalogItem;
using Sisfv.EnvironmentsCatalog.Application.CatalogItems.Commands.UpdateCatalogItem;
using Sisfv.EnvironmentsCatalog.Application.CatalogItems.Commands.UpdateCatalogItemDetail;
using Sisfv.EnvironmentsCatalog.Application.Catalogs.Commands.CreateCatalog;
using Sisfv.EnvironmentsCatalog.Domain.Entities;
using Sisfv.EnvironmentsCatalog.Domain.Enums;
using FluentAssertions;
using NUnit.Framework;

namespace Sisfv.EnvironmentsCatalog.Application.IntegrationTests.CatalogItems.Commands;

using static Testing;

public class UpdateCatalogItemDetailTests : BaseTestFixture
{
    [Test]
    public async Task ShouldRequireValidCatalogItemId()
    {
        var command = new UpdateCatalogItemCommand { Id = 99, Title = "New Title" };
        await FluentActions.Invoking(() => SendAsync(command)).Should().ThrowAsync<NotFoundException>();
    }

    [Test]
    public async Task ShouldUpdateCatalogItem()
    {
        var userId = await RunAsDefaultUserAsync();

        var listId = await SendAsync(new CreateCatalogCommand
        {
            Title = "New List"
        });

        var itemId = await SendAsync(new CreateCatalogItemCommand
        {
            ListId = listId,
            Title = "New Item"
        });

        var command = new UpdateCatalogItemDetailCommand
        {
            Id = itemId,
            ListId = listId,
            Note = "This is the note.",
            Priority = PriorityLevel.High
        };

        await SendAsync(command);

        var item = await FindAsync<CatalogItem>(itemId);

        item.Should().NotBeNull();
        item!.ListId.Should().Be(command.ListId);
        item.Note.Should().Be(command.Note);
        item.Priority.Should().Be(command.Priority);
        item.LastModifiedBy.Should().NotBeNull();
        item.LastModifiedBy.Should().Be(userId);
        item.LastModified.Should().NotBeNull();
        item.LastModified.Should().BeCloseTo(DateTime.Now, TimeSpan.FromMilliseconds(10000));
    }
}
