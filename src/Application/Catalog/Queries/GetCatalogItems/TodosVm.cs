﻿namespace Sisfv.EnvironmentsCatalog.Application.Catalogs.Queries.GetTodos;

public class TodosVm
{
    public IList<PriorityLevelDto> PriorityLevels { get; set; } = new List<PriorityLevelDto>();

    public IList<CatalogDto> Lists { get; set; } = new List<CatalogDto>();
}
