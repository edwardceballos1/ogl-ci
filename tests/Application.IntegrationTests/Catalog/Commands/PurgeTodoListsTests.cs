﻿using Sisfv.EnvironmentsCatalog.Application.Common.Exceptions;
using Sisfv.EnvironmentsCatalog.Application.Common.Security;
using Sisfv.EnvironmentsCatalog.Application.Catalogs.Commands.CreateCatalog;
using Sisfv.EnvironmentsCatalog.Application.Catalogs.Commands.PurgeCatalogs;
using Sisfv.EnvironmentsCatalog.Domain.Entities;
using FluentAssertions;
using NUnit.Framework;

namespace Sisfv.EnvironmentsCatalog.Application.IntegrationTests.Catalogs.Commands;

using static Testing;

public class PurgeCatalogsTests : BaseTestFixture
{
    [Test]
    public async Task ShouldDenyAnonymousUser()
    {
        var command = new PurgeCatalogsCommand();

        command.GetType().Should().BeDecoratedWith<AuthorizeAttribute>();

        var action = () => SendAsync(command);

        await action.Should().ThrowAsync<UnauthorizedAccessException>();
    }

    [Test]
    public async Task ShouldDenyNonAdministrator()
    {
        await RunAsDefaultUserAsync();

        var command = new PurgeCatalogsCommand();

        var action = () => SendAsync(command);

        await action.Should().ThrowAsync<ForbiddenAccessException>();
    }

    [Test]
    public async Task ShouldAllowAdministrator()
    {
        await RunAsAdministratorAsync();

        var command = new PurgeCatalogsCommand();

        var action = () => SendAsync(command);

        await action.Should().NotThrowAsync<ForbiddenAccessException>();
    }

    [Test]
    public async Task ShouldDeleteAllLists()
    {
        await RunAsAdministratorAsync();

        await SendAsync(new CreateCatalogCommand
        {
            Title = "New List #1"
        });

        await SendAsync(new CreateCatalogCommand
        {
            Title = "New List #2"
        });

        await SendAsync(new CreateCatalogCommand
        {
            Title = "New List #3"
        });

        await SendAsync(new PurgeCatalogsCommand());

        var count = await CountAsync<Catalog>();

        count.Should().Be(0);
    }
}
