﻿using Sisfv.EnvironmentsCatalog.Application.Common.Interfaces;
using Sisfv.EnvironmentsCatalog.Domain.Entities;
using Sisfv.EnvironmentsCatalog.Domain.Events;
using MediatR;

namespace Sisfv.EnvironmentsCatalog.Application.CatalogItems.Commands.CreateCatalogItem;

public record CreateCatalogItemCommand : IRequest<int>
{
    public int ListId { get; init; }

    public string? Title { get; init; }
}

public class CreateCatalogItemCommandHandler : IRequestHandler<CreateCatalogItemCommand, int>
{
    private readonly IApplicationDbContext _context;

    public CreateCatalogItemCommandHandler(IApplicationDbContext context)
    {
        _context = context;
    }

    public async Task<int> Handle(CreateCatalogItemCommand request, CancellationToken cancellationToken)
    {
        var entity = new CatalogItem
        {
            ListId = request.ListId,
            Title = request.Title,
            Done = false
        };

        entity.AddDomainEvent(new CatalogItemCreatedEvent(entity));

        _context.CatalogItems.Add(entity);

        await _context.SaveChangesAsync(cancellationToken);

        return entity.Id;
    }
}
