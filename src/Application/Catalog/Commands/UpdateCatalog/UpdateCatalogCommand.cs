﻿using Sisfv.EnvironmentsCatalog.Application.Common.Exceptions;
using Sisfv.EnvironmentsCatalog.Application.Common.Interfaces;
using Sisfv.EnvironmentsCatalog.Domain.Entities;
using MediatR;

namespace Sisfv.EnvironmentsCatalog.Application.Catalogs.Commands.UpdateCatalog;

public record UpdateCatalogCommand : IRequest
{
    public int Id { get; init; }

    public string? Title { get; init; }
}

public class UpdateCatalogCommandHandler : IRequestHandler<UpdateCatalogCommand>
{
    private readonly IApplicationDbContext _context;

    public UpdateCatalogCommandHandler(IApplicationDbContext context)
    {
        _context = context;
    }

    public async Task<Unit> Handle(UpdateCatalogCommand request, CancellationToken cancellationToken)
    {
        var entity = await _context.Catalogs
            .FindAsync(new object[] { request.Id }, cancellationToken);

        if (entity == null)
        {
            throw new NotFoundException(nameof(Catalog), request.Id);
        }

        entity.Title = request.Title;

        await _context.SaveChangesAsync(cancellationToken);

        return Unit.Value;
    }
}
