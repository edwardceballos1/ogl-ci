﻿using Sisfv.EnvironmentsCatalog.Application.Common.Mappings;
using Sisfv.EnvironmentsCatalog.Domain.Entities;

namespace Sisfv.EnvironmentsCatalog.Application.Common.Models;

// Note: This is currently just used to demonstrate applying multiple IMapFrom attributes.
public class LookupDto : IMapFrom<Catalog>, IMapFrom<CatalogItem>
{
    public int Id { get; set; }

    public string? Title { get; set; }
}
