﻿using Sisfv.EnvironmentsCatalog.Application.Common.Exceptions;
using Sisfv.EnvironmentsCatalog.Application.Catalogs.Commands.CreateCatalog;
using Sisfv.EnvironmentsCatalog.Domain.Entities;
using FluentAssertions;
using NUnit.Framework;

namespace Sisfv.EnvironmentsCatalog.Application.IntegrationTests.Catalogs.Commands;

using static Testing;

public class CreateCatalogTests : BaseTestFixture
{
    [Test]
    public async Task ShouldRequireMinimumFields()
    {
        var command = new CreateCatalogCommand();
        await FluentActions.Invoking(() => SendAsync(command)).Should().ThrowAsync<ValidationException>();
    }

    [Test]
    public async Task ShouldRequireUniqueTitle()
    {
        await SendAsync(new CreateCatalogCommand
        {
            Title = "Shopping"
        });

        var command = new CreateCatalogCommand
        {
            Title = "Shopping"
        };

        await FluentActions.Invoking(() =>
            SendAsync(command)).Should().ThrowAsync<ValidationException>();
    }

    [Test]
    public async Task ShouldCreateCatalog()
    {
        var userId = await RunAsDefaultUserAsync();

        var command = new CreateCatalogCommand
        {
            Title = "Tasks"
        };

        var id = await SendAsync(command);

        var list = await FindAsync<Catalog>(id);

        list.Should().NotBeNull();
        list!.Title.Should().Be(command.Title);
        list.CreatedBy.Should().Be(userId);
        list.Created.Should().BeCloseTo(DateTime.Now, TimeSpan.FromMilliseconds(10000));
    }
}
