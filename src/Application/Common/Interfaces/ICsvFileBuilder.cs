﻿using Sisfv.EnvironmentsCatalog.Application.Catalogs.Queries.ExportCatalogItems;

namespace Sisfv.EnvironmentsCatalog.Application.Common.Interfaces;

public interface ICsvFileBuilder
{
    byte[] BuildCatalogItemsFile(IEnumerable<CatalogItemRecord> records);
}
