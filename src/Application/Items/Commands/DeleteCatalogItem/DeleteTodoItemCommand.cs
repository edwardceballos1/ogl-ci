﻿using Sisfv.EnvironmentsCatalog.Application.Common.Exceptions;
using Sisfv.EnvironmentsCatalog.Application.Common.Interfaces;
using Sisfv.EnvironmentsCatalog.Domain.Entities;
using Sisfv.EnvironmentsCatalog.Domain.Events;
using MediatR;

namespace Sisfv.EnvironmentsCatalog.Application.CatalogItems.Commands.DeleteCatalogItem;

public record DeleteCatalogItemCommand(int Id) : IRequest;

public class DeleteCatalogItemCommandHandler : IRequestHandler<DeleteCatalogItemCommand>
{
    private readonly IApplicationDbContext _context;

    public DeleteCatalogItemCommandHandler(IApplicationDbContext context)
    {
        _context = context;
    }

    public async Task<Unit> Handle(DeleteCatalogItemCommand request, CancellationToken cancellationToken)
    {
        var entity = await _context.CatalogItems
            .FindAsync(new object[] { request.Id }, cancellationToken);

        if (entity == null)
        {
            throw new NotFoundException(nameof(CatalogItem), request.Id);
        }

        _context.CatalogItems.Remove(entity);

        entity.AddDomainEvent(new CatalogItemDeletedEvent(entity));

        await _context.SaveChangesAsync(cancellationToken);

        return Unit.Value;
    }
}
