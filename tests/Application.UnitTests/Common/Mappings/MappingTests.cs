﻿using System.Runtime.Serialization;
using AutoMapper;
using Sisfv.EnvironmentsCatalog.Application.Common.Mappings;
using Sisfv.EnvironmentsCatalog.Application.Common.Models;
using Sisfv.EnvironmentsCatalog.Application.Catalogs.Queries.GetTodos;
using Sisfv.EnvironmentsCatalog.Domain.Entities;
using NUnit.Framework;

namespace Sisfv.EnvironmentsCatalog.Application.UnitTests.Common.Mappings;

public class MappingTests
{
    private readonly IConfigurationProvider _configuration;
    private readonly IMapper _mapper;

    public MappingTests()
    {
        _configuration = new MapperConfiguration(config => 
            config.AddProfile<MappingProfile>());

        _mapper = _configuration.CreateMapper();
    }

    [Test]
    public void ShouldHaveValidConfiguration()
    {
        _configuration.AssertConfigurationIsValid();
    }

    [Test]
    [TestCase(typeof(Catalog), typeof(CatalogDto))]
    [TestCase(typeof(CatalogItem), typeof(CatalogItemDto))]
    [TestCase(typeof(Catalog), typeof(LookupDto))]
    [TestCase(typeof(CatalogItem), typeof(LookupDto))]
    public void ShouldSupportMappingFromSourceToDestination(Type source, Type destination)
    {
        var instance = GetInstanceOf(source);

        _mapper.Map(instance, source, destination);
    }

    private object GetInstanceOf(Type type)
    {
        if (type.GetConstructor(Type.EmptyTypes) != null)
            return Activator.CreateInstance(type)!;

        // Type without parameterless constructor
        return FormatterServices.GetUninitializedObject(type);
    }
}
