﻿using AutoMapper;
using AutoMapper.QueryableExtensions;
using Sisfv.EnvironmentsCatalog.Application.Common.Interfaces;
using MediatR;
using Microsoft.EntityFrameworkCore;

namespace Sisfv.EnvironmentsCatalog.Application.Catalogs.Queries.ExportCatalogItems;

public record ExportCatalogItemsQuery : IRequest<ExportCatalogItemsVm>
{
    public int ListId { get; init; }
}

public class ExportCatalogItemsQueryHandler : IRequestHandler<ExportCatalogItemsQuery, ExportCatalogItemsVm>
{
    private readonly IApplicationDbContext _context;
    private readonly IMapper _mapper;
    private readonly ICsvFileBuilder _fileBuilder;

    public ExportCatalogItemsQueryHandler(IApplicationDbContext context, IMapper mapper, ICsvFileBuilder fileBuilder)
    {
        _context = context;
        _mapper = mapper;
        _fileBuilder = fileBuilder;
    }

    public async Task<ExportCatalogItemsVm> Handle(ExportCatalogItemsQuery request, CancellationToken cancellationToken)
    {
        var records = await _context.CatalogItems
                .Where(t => t.ListId == request.ListId)
                .ProjectTo<CatalogItemRecord>(_mapper.ConfigurationProvider)
                .ToListAsync(cancellationToken);

        var vm = new ExportCatalogItemsVm(
            "CatalogItems.csv",
            "text/csv",
            _fileBuilder.BuildCatalogItemsFile(records));

        return vm;
    }
}
