﻿using Sisfv.EnvironmentsCatalog.Domain.Entities;
using Microsoft.EntityFrameworkCore;

namespace Sisfv.EnvironmentsCatalog.Application.Common.Interfaces;

public interface IApplicationDbContext
{
    DbSet<Catalog> Catalogs { get; }

    DbSet<CatalogItem> CatalogItems { get; }

    Task<int> SaveChangesAsync(CancellationToken cancellationToken);
}
