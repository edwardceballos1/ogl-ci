﻿using Sisfv.EnvironmentsCatalog.Application.Common.Exceptions;
using Sisfv.EnvironmentsCatalog.Application.Common.Interfaces;
using Sisfv.EnvironmentsCatalog.Domain.Entities;
using Sisfv.EnvironmentsCatalog.Domain.Enums;
using MediatR;

namespace Sisfv.EnvironmentsCatalog.Application.CatalogItems.Commands.UpdateCatalogItemDetail;

public record UpdateCatalogItemDetailCommand : IRequest
{
    public int Id { get; init; }

    public int ListId { get; init; }

    public PriorityLevel Priority { get; init; }

    public string? Note { get; init; }
}

public class UpdateCatalogItemDetailCommandHandler : IRequestHandler<UpdateCatalogItemDetailCommand>
{
    private readonly IApplicationDbContext _context;

    public UpdateCatalogItemDetailCommandHandler(IApplicationDbContext context)
    {
        _context = context;
    }

    public async Task<Unit> Handle(UpdateCatalogItemDetailCommand request, CancellationToken cancellationToken)
    {
        var entity = await _context.CatalogItems
            .FindAsync(new object[] { request.Id }, cancellationToken);

        if (entity == null)
        {
            throw new NotFoundException(nameof(CatalogItem), request.Id);
        }

        entity.ListId = request.ListId;
        entity.Priority = request.Priority;
        entity.Note = request.Note;

        await _context.SaveChangesAsync(cancellationToken);

        return Unit.Value;
    }
}
