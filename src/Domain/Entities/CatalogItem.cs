﻿namespace Sisfv.EnvironmentsCatalog.Domain.Entities;

public class CatalogItem : BaseAuditableEntity
{
    public int ListId { get; set; }

    public string? Title { get; set; }

    public string? Note { get; set; }

    public PriorityLevel Priority { get; set; }

    public DateTime? Reminder { get; set; }

    private bool _done;
    public bool Done
    {
        get => _done;
        set
        {
            if (value == true && _done == false)
            {
                AddDomainEvent(new CatalogItemCompletedEvent(this));
            }

            _done = value;
        }
    }

    public Catalog List { get; set; } = null!;
}
