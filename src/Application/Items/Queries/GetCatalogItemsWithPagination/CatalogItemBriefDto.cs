﻿using Sisfv.EnvironmentsCatalog.Application.Common.Mappings;
using Sisfv.EnvironmentsCatalog.Domain.Entities;

namespace Sisfv.EnvironmentsCatalog.Application.CatalogItems.Queries.GetCatalogItemsWithPagination;

public class CatalogItemBriefDto : IMapFrom<CatalogItem>
{
    public int Id { get; set; }

    public int ListId { get; set; }

    public string? Title { get; set; }

    public bool Done { get; set; }
}
