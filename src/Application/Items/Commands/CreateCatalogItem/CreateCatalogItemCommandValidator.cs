﻿using FluentValidation;

namespace Sisfv.EnvironmentsCatalog.Application.CatalogItems.Commands.CreateCatalogItem;

public class CreateCatalogItemCommandValidator : AbstractValidator<CreateCatalogItemCommand>
{
    public CreateCatalogItemCommandValidator()
    {
        RuleFor(v => v.Title)
            .MaximumLength(200)
            .NotEmpty();
    }
}
