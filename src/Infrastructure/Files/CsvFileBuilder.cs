﻿using System.Globalization;
using Sisfv.EnvironmentsCatalog.Application.Common.Interfaces;
using Sisfv.EnvironmentsCatalog.Infrastructure.Files.Maps;
using CsvHelper;
using Sisfv.EnvironmentsCatalog.Application.Catalogs.Queries.ExportCatalogItems;

namespace Sisfv.EnvironmentsCatalog.Infrastructure.Files;

public class CsvFileBuilder : ICsvFileBuilder
{
    public byte[] BuildCatalogItemsFile(IEnumerable<CatalogItemRecord> records)
    {
        using var memoryStream = new MemoryStream();
        using (var streamWriter = new StreamWriter(memoryStream))
        {
            using var csvWriter = new CsvWriter(streamWriter, CultureInfo.InvariantCulture);

            csvWriter.Configuration.RegisterClassMap<CatalogItemRecordMap>();
            csvWriter.WriteRecords(records);
        }

        return memoryStream.ToArray();
    }
}
