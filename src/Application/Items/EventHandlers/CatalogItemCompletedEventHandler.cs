﻿using Sisfv.EnvironmentsCatalog.Domain.Events;
using MediatR;
using Microsoft.Extensions.Logging;

namespace Sisfv.EnvironmentsCatalog.Application.CatalogItems.EventHandlers;

public class CatalogItemCompletedEventHandler : INotificationHandler<CatalogItemCompletedEvent>
{
    private readonly ILogger<CatalogItemCompletedEventHandler> _logger;

    public CatalogItemCompletedEventHandler(ILogger<CatalogItemCompletedEventHandler> logger)
    {
        _logger = logger;
    }

    public Task Handle(CatalogItemCompletedEvent notification, CancellationToken cancellationToken)
    {
        _logger.LogInformation("CleanArchitecture Domain Event: {DomainEvent}", notification.GetType().Name);

        return Task.CompletedTask;
    }
}
