﻿using Microsoft.AspNetCore.Identity;

namespace Sisfv.EnvironmentsCatalog.Infrastructure.Identity;

public class ApplicationUser : IdentityUser
{
}
