﻿using Sisfv.EnvironmentsCatalog.Application.Common.Exceptions;
using Sisfv.EnvironmentsCatalog.Application.CatalogItems.Commands.CreateCatalogItem;
using Sisfv.EnvironmentsCatalog.Application.CatalogItems.Commands.DeleteCatalogItem;
using Sisfv.EnvironmentsCatalog.Application.Catalogs.Commands.CreateCatalog;
using Sisfv.EnvironmentsCatalog.Domain.Entities;
using FluentAssertions;
using NUnit.Framework;

namespace Sisfv.EnvironmentsCatalog.Application.IntegrationTests.CatalogItems.Commands;

using static Testing;

public class DeleteCatalogItemTests : BaseTestFixture
{
    [Test]
    public async Task ShouldRequireValidCatalogItemId()
    {
        var command = new DeleteCatalogItemCommand(99);

        await FluentActions.Invoking(() =>
            SendAsync(command)).Should().ThrowAsync<NotFoundException>();
    }

    [Test]
    public async Task ShouldDeleteCatalogItem()
    {
        var listId = await SendAsync(new CreateCatalogCommand
        {
            Title = "New List"
        });

        var itemId = await SendAsync(new CreateCatalogItemCommand
        {
            ListId = listId,
            Title = "New Item"
        });

        await SendAsync(new DeleteCatalogItemCommand(itemId));

        var item = await FindAsync<CatalogItem>(itemId);

        item.Should().BeNull();
    }
}
