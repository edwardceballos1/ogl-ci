﻿using Sisfv.EnvironmentsCatalog.Application.Common.Models;
using Sisfv.EnvironmentsCatalog.Application.CatalogItems.Commands.CreateCatalogItem;
using Sisfv.EnvironmentsCatalog.Application.CatalogItems.Commands.DeleteCatalogItem;
using Sisfv.EnvironmentsCatalog.Application.CatalogItems.Commands.UpdateCatalogItem;
using Sisfv.EnvironmentsCatalog.Application.CatalogItems.Commands.UpdateCatalogItemDetail;
using Sisfv.EnvironmentsCatalog.Application.CatalogItems.Queries.GetCatalogItemsWithPagination;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace Sisfv.EnvironmentsCatalog.Api.Controllers;

[Authorize]
public class CatalogItemsController : ApiControllerBase
{
    [HttpGet]
    public async Task<ActionResult<PaginatedList<CatalogItemBriefDto>>> GetCatalogItemsWithPagination([FromQuery] GetCatalogItemsWithPaginationQuery query)
    {
        return await Mediator.Send(query);
    }

    [HttpPost]
    public async Task<ActionResult<int>> Create(CreateCatalogItemCommand command)
    {
        return await Mediator.Send(command);
    }

    [HttpPut("{id}")]
    public async Task<ActionResult> Update(int id, UpdateCatalogItemCommand command)
    {
        if (id != command.Id)
        {
            return BadRequest();
        }

        await Mediator.Send(command);

        return NoContent();
    }

    [HttpPut("[action]")]
    public async Task<ActionResult> UpdateItemDetails(int id, UpdateCatalogItemDetailCommand command)
    {
        if (id != command.Id)
        {
            return BadRequest();
        }

        await Mediator.Send(command);

        return NoContent();
    }

    [HttpDelete("{id}")]
    public async Task<ActionResult> Delete(int id)
    {
        await Mediator.Send(new DeleteCatalogItemCommand(id));

        return NoContent();
    }
}
