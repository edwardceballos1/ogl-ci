﻿namespace Sisfv.EnvironmentsCatalog.Application.Catalogs.Queries.GetTodos;

public class PriorityLevelDto
{
    public int Value { get; set; }

    public string? Name { get; set; }
}
