﻿using Sisfv.EnvironmentsCatalog.Application.Common.Interfaces;
using Sisfv.EnvironmentsCatalog.Domain.Entities;
using MediatR;

namespace Sisfv.EnvironmentsCatalog.Application.Catalogs.Commands.CreateCatalog;

public record CreateCatalogCommand : IRequest<int>
{
    public string? Title { get; init; }
}

public class CreateCatalogCommandHandler : IRequestHandler<CreateCatalogCommand, int>
{
    private readonly IApplicationDbContext _context;

    public CreateCatalogCommandHandler(IApplicationDbContext context)
    {
        _context = context;
    }

    public async Task<int> Handle(CreateCatalogCommand request, CancellationToken cancellationToken)
    {
        var entity = new Catalog();

        entity.Title = request.Title;

        _context.Catalogs.Add(entity);

        await _context.SaveChangesAsync(cancellationToken);

        return entity.Id;
    }
}
