﻿namespace Sisfv.EnvironmentsCatalog.Domain.Events;

public class CatalogItemCreatedEvent : BaseEvent
{
    public CatalogItemCreatedEvent(CatalogItem item)
    {
        Item = item;
    }

    public CatalogItem Item { get; }
}
