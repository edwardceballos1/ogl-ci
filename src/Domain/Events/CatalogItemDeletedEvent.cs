﻿namespace Sisfv.EnvironmentsCatalog.Domain.Events;

public class CatalogItemDeletedEvent : BaseEvent
{
    public CatalogItemDeletedEvent(CatalogItem item)
    {
        Item = item;
    }

    public CatalogItem Item { get; }
}
