﻿using AutoMapper;
using AutoMapper.QueryableExtensions;
using Sisfv.EnvironmentsCatalog.Application.Common.Interfaces;
using Sisfv.EnvironmentsCatalog.Application.Common.Mappings;
using Sisfv.EnvironmentsCatalog.Application.Common.Models;
using MediatR;

namespace Sisfv.EnvironmentsCatalog.Application.CatalogItems.Queries.GetCatalogItemsWithPagination;

public record GetCatalogItemsWithPaginationQuery : IRequest<PaginatedList<CatalogItemBriefDto>>
{
    public int ListId { get; init; }
    public int PageNumber { get; init; } = 1;
    public int PageSize { get; init; } = 10;
}

public class GetCatalogItemsWithPaginationQueryHandler : IRequestHandler<GetCatalogItemsWithPaginationQuery, PaginatedList<CatalogItemBriefDto>>
{
    private readonly IApplicationDbContext _context;
    private readonly IMapper _mapper;

    public GetCatalogItemsWithPaginationQueryHandler(IApplicationDbContext context, IMapper mapper)
    {
        _context = context;
        _mapper = mapper;
    }

    public async Task<PaginatedList<CatalogItemBriefDto>> Handle(GetCatalogItemsWithPaginationQuery request, CancellationToken cancellationToken)
    {
        return await _context.CatalogItems
            .Where(x => x.ListId == request.ListId)
            .OrderBy(x => x.Title)
            .ProjectTo<CatalogItemBriefDto>(_mapper.ConfigurationProvider)
            .PaginatedListAsync(request.PageNumber, request.PageSize);
    }
}
