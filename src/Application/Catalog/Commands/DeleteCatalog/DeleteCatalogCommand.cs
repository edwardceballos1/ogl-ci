﻿using Sisfv.EnvironmentsCatalog.Application.Common.Exceptions;
using Sisfv.EnvironmentsCatalog.Application.Common.Interfaces;
using Sisfv.EnvironmentsCatalog.Domain.Entities;
using MediatR;
using Microsoft.EntityFrameworkCore;

namespace Sisfv.EnvironmentsCatalog.Application.Catalogs.Commands.DeleteCatalog;

public record DeleteCatalogCommand(int Id) : IRequest;

public class DeleteCatalogCommandHandler : IRequestHandler<DeleteCatalogCommand>
{
    private readonly IApplicationDbContext _context;

    public DeleteCatalogCommandHandler(IApplicationDbContext context)
    {
        _context = context;
    }

    public async Task<Unit> Handle(DeleteCatalogCommand request, CancellationToken cancellationToken)
    {
        var entity = await _context.Catalogs
            .Where(l => l.Id == request.Id)
            .SingleOrDefaultAsync(cancellationToken);

        if (entity == null)
        {
            throw new NotFoundException(nameof(Catalog), request.Id);
        }

        _context.Catalogs.Remove(entity);

        await _context.SaveChangesAsync(cancellationToken);

        return Unit.Value;
    }
}
