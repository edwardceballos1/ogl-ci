﻿using AutoMapper;
using Sisfv.EnvironmentsCatalog.Application.Common.Mappings;
using Sisfv.EnvironmentsCatalog.Domain.Entities;

namespace Sisfv.EnvironmentsCatalog.Application.Catalogs.Queries.GetTodos;

public class CatalogItemDto : IMapFrom<CatalogItem>
{
    public int Id { get; set; }

    public int ListId { get; set; }

    public string? Title { get; set; }

    public bool Done { get; set; }

    public int Priority { get; set; }

    public string? Note { get; set; }

    public void Mapping(Profile profile)
    {
        profile.CreateMap<CatalogItem, CatalogItemDto>()
            .ForMember(d => d.Priority, opt => opt.MapFrom(s => (int)s.Priority));
    }
}
