﻿using Sisfv.EnvironmentsCatalog.Application.Common.Interfaces;
using FluentValidation;
using Microsoft.EntityFrameworkCore;

namespace Sisfv.EnvironmentsCatalog.Application.Catalogs.Commands.CreateCatalog;

public class CreateCatalogCommandValidator : AbstractValidator<CreateCatalogCommand>
{
    private readonly IApplicationDbContext _context;

    public CreateCatalogCommandValidator(IApplicationDbContext context)
    {
        _context = context;

        RuleFor(v => v.Title)
            .NotEmpty().WithMessage("Title is required.")
            .MaximumLength(200).WithMessage("Title must not exceed 200 characters.")
            .MustAsync(BeUniqueTitle).WithMessage("The specified title already exists.");
    }

    public async Task<bool> BeUniqueTitle(string title, CancellationToken cancellationToken)
    {
        return await _context.Catalogs
            .AllAsync(l => l.Title != title, cancellationToken);
    }
}
