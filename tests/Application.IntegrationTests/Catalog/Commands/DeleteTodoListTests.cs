﻿using Sisfv.EnvironmentsCatalog.Application.Common.Exceptions;
using Sisfv.EnvironmentsCatalog.Application.Catalogs.Commands.CreateCatalog;
using Sisfv.EnvironmentsCatalog.Application.Catalogs.Commands.DeleteCatalog;
using Sisfv.EnvironmentsCatalog.Domain.Entities;
using FluentAssertions;
using NUnit.Framework;

namespace Sisfv.EnvironmentsCatalog.Application.IntegrationTests.Catalogs.Commands;

using static Testing;

public class DeleteCatalogTests : BaseTestFixture
{
    [Test]
    public async Task ShouldRequireValidCatalogId()
    {
        var command = new DeleteCatalogCommand(99);
        await FluentActions.Invoking(() => SendAsync(command)).Should().ThrowAsync<NotFoundException>();
    }

    [Test]
    public async Task ShouldDeleteCatalog()
    {
        var listId = await SendAsync(new CreateCatalogCommand
        {
            Title = "New List"
        });

        await SendAsync(new DeleteCatalogCommand(listId));

        var list = await FindAsync<Catalog>(listId);

        list.Should().BeNull();
    }
}
