﻿using Sisfv.EnvironmentsCatalog.Application.Common.Interfaces;
using Sisfv.EnvironmentsCatalog.Application.Common.Security;
using MediatR;

namespace Sisfv.EnvironmentsCatalog.Application.Catalogs.Commands.PurgeCatalogs;

[Authorize(Roles = "Administrator")]
[Authorize(Policy = "CanPurge")]
public record PurgeCatalogsCommand : IRequest;

public class PurgeCatalogsCommandHandler : IRequestHandler<PurgeCatalogsCommand>
{
    private readonly IApplicationDbContext _context;

    public PurgeCatalogsCommandHandler(IApplicationDbContext context)
    {
        _context = context;
    }

    public async Task<Unit> Handle(PurgeCatalogsCommand request, CancellationToken cancellationToken)
    {
        _context.Catalogs.RemoveRange(_context.Catalogs);

        await _context.SaveChangesAsync(cancellationToken);

        return Unit.Value;
    }
}
