﻿using Sisfv.EnvironmentsCatalog.Application.Catalogs.Commands.CreateCatalog;
using Sisfv.EnvironmentsCatalog.Application.Catalogs.Commands.DeleteCatalog;
using Sisfv.EnvironmentsCatalog.Application.Catalogs.Commands.UpdateCatalog;
using Sisfv.EnvironmentsCatalog.Application.Catalogs.Queries.ExportCatalogItems;
using Sisfv.EnvironmentsCatalog.Application.Catalogs.Queries.GetTodos;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace Sisfv.EnvironmentsCatalog.Api.Controllers;

[Authorize]
public class CatalogsController : ApiControllerBase
{
    [HttpGet]
    public async Task<ActionResult<TodosVm>> Get()
    {
        return await Mediator.Send(new GetTodosQuery());
    }

    [HttpGet("{id}")]
    public async Task<FileResult> Get(int id)
    {
        var vm = await Mediator.Send(new ExportCatalogItemsQuery { ListId = id });

        return File(vm.Content, vm.ContentType, vm.FileName);
    }

    [HttpPost]
    public async Task<ActionResult<int>> Create(CreateCatalogCommand command)
    {
        return await Mediator.Send(command);
    }

    [HttpPut("{id}")]
    public async Task<ActionResult> Update(int id, UpdateCatalogCommand command)
    {
        if (id != command.Id)
        {
            return BadRequest();
        }

        await Mediator.Send(command);

        return NoContent();
    }

    [HttpDelete("{id}")]
    public async Task<ActionResult> Delete(int id)
    {
        await Mediator.Send(new DeleteCatalogCommand(id));

        return NoContent();
    }
}
