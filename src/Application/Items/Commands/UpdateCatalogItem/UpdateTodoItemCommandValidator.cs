﻿using FluentValidation;

namespace Sisfv.EnvironmentsCatalog.Application.CatalogItems.Commands.UpdateCatalogItem;

public class UpdateCatalogItemCommandValidator : AbstractValidator<UpdateCatalogItemCommand>
{
    public UpdateCatalogItemCommandValidator()
    {
        RuleFor(v => v.Title)
            .MaximumLength(200)
            .NotEmpty();
    }
}
